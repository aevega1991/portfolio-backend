package com.vega;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class HelloServiceTest {

    @Test
    public void testHelloEndpoint() {
        given()
          .when().get("/hello/polite/Baeldung")
          .then()
             .statusCode(200)
             .body(is("Good morning Baeldung"));
    }

}