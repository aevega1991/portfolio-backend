package com.vega;

import io.quarkus.test.junit.NativeImageTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@NativeImageTest
public class NativeHelloServiceIT extends HelloServiceTest {

    // Execute the same tests but in native mode.
    @Test
    public void testHelloEndpoint() {
        given()
                .when().get("/hello/polite/Baeldung")
                .then()
                .statusCode(200)
                .body(is("Good morning Baeldung"));
    }
}